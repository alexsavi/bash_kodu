#!/bin/bash
# Autor: Alex Savi, 2016
 
# Skriptimiskeeled 1. kodutöö (bash)

# Mida see skript teeb?
# paigaldab samba (see osa pole kohustuslik)
# loob kausta KAUST (kui vaja)
# loob grupi GRUPP (kui vaja)
# lisab grupile sobivad read smb.conf faili ja teeb failiserveri teenusele reload'i

# Väljumiskoodid:
# 1 - ei ole juurkasutaja
# 2 - vale arv parameetreid
# 3 - samba reload õnnestus

# v0.1 - Parameetrite arvu kontroll
# v0.2 - Juurkasutaja kontroll
# v0.3 - Kaust ja grupp
# v0.4 - Samba

# Skript on poolik!!!

#Kontrollime kas kasutaja on juurkasutaja /slaid nr. 20
if [ $UID -ne 0 ]
then
    echo "Käivita skript $0 root õigustes!"
    exit 1
else
    echo "Oled juurkasutaja!"
fi
#Kontrollib parameetrite arvu, kas on 2 või 3
if [ $# -ne 2 ] && [ $# -ne 3 ]; then
    echo "Kasutamine: $0 KAUST GRUPP <JAGATUD KAUST>"
    exit 1
fi

KAUST=$1
KAUSTBASE=$(basename $KAUST)
GRUPP=$2
JAGKAUST=${3-$KAUSTBASE}

echo "KAUST=$KAUST"
echo "KAUSTBASE=$KAUSTBASE"
echo "GRUPP=$GRUPP"
echo "JAGKAUST=$JAGKAUST"

#Kontrollime kausta olemasolu, kui vaja loome kausta
#if [ -d $KAUST ]
#then
#    echo "Kaust on olemas"
#else
#    mkdir -p $KAUST
#    exit 1
#fi
[ ! -d $KAUST ] && mkdir $KAUST

#Kontrollime grupi olemasolu, kui vaja loome grupi. Kasutan nurksulge, et ei toimuks käsurea enneaegset printimist.
[ $(getent group $GRUPP) ] || groupadd $GRUPP

#Samba konfifailist varukoopia
sudo cp /etc/samba/smb.conf /etc/samba/smb.conf.old

#Samba konfifaili kirjutamine
cat >> /etc/samba/samba.conf << LOPP

LOPP

#Samba teenuse reload
service smbd reload > /dev/null

exit 0
